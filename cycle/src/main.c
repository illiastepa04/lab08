/**
 * # Загальне завдання
 * 1. Переробити програми, що були розроблені раніше
 * таким чином, щоб у них використовувались функції
 * @author Stepa Illia.
 * @date 20-jan-2021
 */





#include <stdlib.h>
//Використовуємо заголовочний файл для роботи з printf
#include <stdio.h>
#include <stdbool.h>
//Використовуємо заголовочний файл для роботи з srand(time(NULL));
#include <time.h>
#include "main.h"


bool is_number_perfect(int number);



//Точка входу у программу
int main(){

	//Потрібно для роботи функції rand
	srand(time(NULL));
	
	//Створємо рандомні значення зміної х до 20
	int x = rand() % 20;

	if(is_number_perfect(x)){
		printf("Число досконале %d\n", x);
	} else{
		printf("Число не досконале %d\n", x);
	}
	
	return 0;
}



///Функція для визначення досконалого числа
bool is_number_perfect(int number){


	int sum = 0;
 /** Для того, щоб визначити досконале число, нам потрібно додати дільники заданого числа (окрім заданого числа, тому й ставимо границю  "i < number") 
  */
	for(int i = 1; i < number; ++i){
		if(number % i == 0){
			sum += i;
		}
	}		
/// На виході з функції отримуємо суму


	if(sum == number){
		return true;
	}
	
	return false;
}
